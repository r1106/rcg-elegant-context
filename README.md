# Run project locally

Clone Project from Gitlab

## `git clone git@gitlab.com:r1106/rcg-elegant-context.git`

# Installation

Install missing packages in the root folder

## `npm install`

# Start project in development mode

In the project directory, you can run:

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

# Pages Overview

![Home Page Screen](src/assets/images/readme/react_elegant_01.png "Home Page Screen")

![Cart Page Screen](src/assets/images/readme/react_elegant_02.png "Cart Page Screen")
