import { Dispatch, useReducer } from "react";

import Header from "./components/Header/Header";
import Shop from "./components/Shop/Shop";
import { SHOPPING_CART_CONTEXT } from "./store/shopping-context";
import { shoppingReducer } from "./store/shopping-reducer";
import { INIT_ITEM } from "./types/consts/init-item.const";
import { ReducerTypesEnum } from "./types/enums/reducer-types.enum";
import { IAction } from "./types/interfaces/action.interface";
import { IState } from "./types/interfaces/state.interface";

function App(): JSX.Element {
  const [shoppingState, dispatchShoppingState]: [
    shoppingState: IState,
    dispatchShoppingState: Dispatch<IAction>
  ] = useReducer(shoppingReducer, {
    items: [INIT_ITEM],
  });

  function handleAddItemToCart(id: string) {
    dispatchShoppingState({
      type: ReducerTypesEnum.AddItem,
      payload: { productId: id },
    });
  }

  function handleUpdateCartItemQuantity(
    productId: string,
    amount: number
  ): void {
    dispatchShoppingState({
      type: ReducerTypesEnum.UpdateCartItem,
      payload: { productId, amount },
    });
  }

  const shoppingContext = {
    cart: shoppingState,
    onUpdateCartItemQuantity: handleUpdateCartItemQuantity,
    onAddItemToCart: handleAddItemToCart,
  };

  return (
    <SHOPPING_CART_CONTEXT.Provider value={shoppingContext}>
      <Header />
      <Shop />
    </SHOPPING_CART_CONTEXT.Provider>
  );
}

export default App;
