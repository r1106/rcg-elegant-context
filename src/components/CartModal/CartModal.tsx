import { useEffect, useRef } from "react";
import { createPortal } from "react-dom";
import { IItem } from "../../types/interfaces/item.interface";
import Cart from "../Cart/Cart";

interface IModalType {
  cartItems: IItem[];
  onUpdateCartItemQuantity: (id: string, amount: number) => void;
  title: string;
  actions: JSX.Element;
  openModal: boolean;
}

function CartModal({
  cartItems,
  onUpdateCartItemQuantity,
  title,
  actions,
  openModal,
}: IModalType): JSX.Element {
  const dialog = useRef<HTMLDialogElement>(null);

  useEffect(() => {
    const modal = dialog.current;
    if (openModal) {
      modal?.showModal();
    }

    return () => modal?.close();
  }, [openModal]);

  return createPortal(
    <dialog id="modal" ref={dialog} open={openModal}>
      <h2>{title}</h2>
      <Cart items={cartItems} onUpdateItemQuantity={onUpdateCartItemQuantity} />
      <form method="dialog" id="modal-actions">
        {actions}
      </form>
    </dialog>,
    document.getElementById("modal") as HTMLElement
  );
}

export default CartModal;
