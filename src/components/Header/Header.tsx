import { Dispatch, SetStateAction, useContext, useState } from "react";
import logo from "../../assets/logo.png";
import { SHOPPING_CART_CONTEXT } from "../../store/shopping-context";
import CartModal from "../CartModal/CartModal";

export default function Header(): JSX.Element {
  const { cart, onUpdateCartItemQuantity } = useContext(SHOPPING_CART_CONTEXT);

  const [modalIsOpen, setModalIsOpen]: [
    modalIsOpen: boolean,
    setModalIsOpen: Dispatch<SetStateAction<boolean>>
  ] = useState(false);

  const cartQuantity = cart.items.length;

  function handleOpenCartClick(): void {
    setModalIsOpen(true);
  }

  function handleModalCloseClick(): void {
    setModalIsOpen(false);
  }

  function handleModalCheckoutClick(): void {
    setModalIsOpen(false);
  }

  const closeButton = <button onClick={handleModalCloseClick}>Close</button>;

  let modalActions = closeButton;

  if (cartQuantity > 0) {
    modalActions = (
      <>
        {closeButton}
        <button onClick={handleModalCheckoutClick}>Checkout</button>
      </>
    );
  }

  return (
    <>
      <CartModal
        cartItems={cart.items}
        onUpdateCartItemQuantity={onUpdateCartItemQuantity}
        title="Your Cart"
        actions={modalActions}
        openModal={modalIsOpen}
      />
      <header id="main-header">
        <div id="main-title">
          <img src={logo} alt="Elegant model" />
          <h1>Elegant Context</h1>
        </div>
        <p>
          <button onClick={handleOpenCartClick}>Cart ({cartQuantity})</button>
        </p>
      </header>
    </>
  );
}
