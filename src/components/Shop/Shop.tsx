import { useContext } from "react";
import { SHOPPING_CART_CONTEXT } from "../../store/shopping-context";
import { DUMMY_PRODUCTS } from "../../types/consts/dummy-products.const";
import Product from "../Product/Product";

export default function Shop() {
  const { onAddItemToCart } = useContext(SHOPPING_CART_CONTEXT);

  return (
    <section id="shop">
      <h2>Elegant Clothing For Everyone</h2>

      <ul id="products">
        {DUMMY_PRODUCTS.map((product) => (
          <li key={product.id}>
            <Product {...product} onAddToCart={onAddItemToCart} />
          </li>
        ))}
      </ul>
    </section>
  );
}
