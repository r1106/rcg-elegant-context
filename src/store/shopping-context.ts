import { Context, createContext } from "react";
import { IContext } from "../types/interfaces/context.interface";

export const SHOPPING_CART_CONTEXT: Context<IContext> = createContext({
  cart: {
    items: [
      {
        id: "",
        name: "",
        price: 0,
        quantity: 0,
      },
    ],
  },
  onUpdateCartItemQuantity: (productId: string, amount: number) => {},
  onAddItemToCart: (productId: string) => {},
});
