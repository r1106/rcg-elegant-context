import { DUMMY_PRODUCTS } from "../types/consts/dummy-products.const";
import { ReducerTypesEnum } from "../types/enums/reducer-types.enum";
import { IAction } from "../types/interfaces/action.interface";
import { IState } from "../types/interfaces/state.interface";

export function shoppingReducer(state: IState, action: IAction): IState {
  const { type, payload } = action;

  if (type === ReducerTypesEnum.AddItem) {
    const updatedItems = [...state.items];

    const existingCartItemIndex = updatedItems.findIndex(
      (cartItem) => cartItem.id === payload.productId
    );
    const existingCartItem = updatedItems[existingCartItemIndex];

    if (existingCartItem) {
      const updatedItem = {
        ...existingCartItem,
        quantity: existingCartItem.quantity + 1,
      };
      updatedItems[existingCartItemIndex] = updatedItem;
    } else {
      const product = DUMMY_PRODUCTS.find(
        (product) => product.id === payload.productId
      );
      if (product === undefined) {
        return { ...state };
      }

      updatedItems.push({
        id: payload.productId,
        name: product.title,
        price: product.price,
        quantity: 1,
      });
    }

    return {
      ...state,
      items: updatedItems,
    };
  }

  if (type === ReducerTypesEnum.UpdateCartItem) {
    const updatedItems = [...state.items];
    const updatedItemIndex = updatedItems.findIndex(
      (item) => item.id === payload.productId
    );

    const updatedItem = {
      ...updatedItems[updatedItemIndex],
    };

    if (payload.amount !== undefined) {
      updatedItem.quantity += payload.amount;
    }

    if (updatedItem.quantity <= 0) {
      updatedItems.splice(updatedItemIndex, 1);
    } else {
      updatedItems[updatedItemIndex] = updatedItem;
    }

    return {
      ...state,
      items: updatedItems,
    };
  }

  return state;
}
