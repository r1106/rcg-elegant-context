import { DUMMY_PRODUCTS } from "./dummy-products.const";

export const INIT_ITEM = {
  id: DUMMY_PRODUCTS[0].id,
  name: DUMMY_PRODUCTS[0].title,
  price: DUMMY_PRODUCTS[0].price,
  quantity: 1,
};
