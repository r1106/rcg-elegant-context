export enum ReducerTypesEnum {
  AddItem = "ADD_ITEM",
  UpdateCartItem = "UPDATE_CART_ITEM",
}
