import { ReducerTypesEnum } from "../enums/reducer-types.enum";

export interface IAction {
  type: ReducerTypesEnum;
  payload: {
    productId: string;
    amount?: number;
  };
}
