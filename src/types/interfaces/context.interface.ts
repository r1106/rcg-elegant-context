import { ICart } from "./cart.interface";

export interface IContext {
  cart: ICart;
  onUpdateCartItemQuantity: (productId: string, amount: number) => void;
  onAddItemToCart: (productId: string) => void;
}
