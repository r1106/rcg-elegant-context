import { IItem } from "./item.interface";

export interface IState {
  items: IItem[];
}
